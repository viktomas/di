import { Container, Injectable, brandInstance, createInterfaceId } from './dependency';

interface Logger {
  log(...args: unknown[]): void;
}

interface A {
  field: string;
}

interface B {
  hello(): string;
}

interface C {
  world(): string;
}

const Logger = createInterfaceId<Logger>('Logger');
const A = createInterfaceId<A>('A');
const B = createInterfaceId<B>('B');
const C = createInterfaceId<C>('C');

@Injectable(A, [])
class AImpl implements A {
  field = 'value';
}

@Injectable(B, [A])
class BImpl implements B {
  #a: A;

  constructor(a: A) {
    this.#a = a;
  }

  hello() {
    return `B.hello says A.field = ${this.#a.field}`;
  }
}

@Injectable(C, [A, B])
class CImpl implements C {
  #a: A;
  #b: B;

  constructor(a: A, b: B) {
    this.#a = a;
    this.#b = b;
  }

  world() {
    return `C.world says A.field = ${this.#a.field}, B.hello = ${this.#b.hello()}`;
  }
}

const container = new Container();
container.instantiate(AImpl, BImpl, CImpl);

const customLogger: Logger = {
  log: (...args) => console.log('custom logger', ...args),
};

container.addInstances(brandInstance(Logger, customLogger));

const b = container.get(B);
const c = container.get(C);

container.get(Logger).log(b.hello());
container.get(Logger).log(c.world());
