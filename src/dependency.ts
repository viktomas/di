import { groupBy } from './group_by';

/**
 * This is a branded type for the type T, it associates a string with the type so we
 * can refer to the T type at runtime.
 */
export type InterfaceId<T> = string & { __type: T };

const getRandomString = () => Math.random().toString(36).substring(2, 15);
/**
 * Creates a runtime identifier of an interface used for dependency injection.
 *
 * Every call to this function produces unique identifier, you can't call this method twice for the same Type!
 */
export const createInterfaceId = <T>(id: string): InterfaceId<T> => `${id}-${getRandomString()}` as InterfaceId<T>;

/*
 * Takes an array of InterfaceIDs with unknown type and turn them into an array of the types that the InterfaceIds wrap
 *
 * e.g. `UnwrapInterfaceIds<[InterfaceId<string>, InterfaceId<number>]>` equals to `[string, number]`
 *
 * this type is used to enforce dependency types in the constructor of the injected class
 */
type UnwrapInterfaceIds<T extends InterfaceId<unknown>[]> = {
  [K in keyof T]: T[K] extends InterfaceId<infer U> ? U : never;
};

interface Metadata {
  id: string;
  dependencies: string[];
}

/**
 * Here we store the id and dependencies for all classes decorated with @Injectable
 */
const injectableMetadata = new WeakMap<object, Metadata>();

/**
 * Decorate your class with @Injectable(id, dependencies)
 * param id = InterfaceId of the interface implemented by your class (use createInterfaceId to create one)
 * param dependencies = List of InterfaceIds that will be injected into class' constructor
 */
export function Injectable<I, TDependencies extends InterfaceId<unknown>[]>(
  id: InterfaceId<I>,
  dependencies: [...TDependencies], // we can add  `| [] = [],` to make dependencies optional, but the type checker messages are quite cryptic when the decorated class has some constructor arguments
) {
  // this is the trickiest part of the whole DI framework
  // we say, this decorator takes
  // - id (the interface that the injectable implements)
  // - dependencies (list of interface ids that will be injected to constructor)
  //
  // With then we return function that ensures that the decorated class implements the id interface
  // and its constructor has arguments of same type and order as the dependencies argument to the decorator
  return function <T extends { new (...args: UnwrapInterfaceIds<TDependencies>): I }>(
    constructor: T,
    { kind }: ClassDecoratorContext,
  ) {
    if (kind === 'class') {
      injectableMetadata.set(constructor, { id, dependencies: dependencies || [] });
      return constructor;
    }
    throw new Error('Injectable decorator can only be used on a class.');
  };
}

// new (...args: any[]): any is actually how TS defines type of a class
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type WithConstructor = { new (...args: any[]): any; name: string };
type ClassWithDependencies = { cls: WithConstructor; id: string; dependencies: string[] };

type Validator = (cwds: ClassWithDependencies[], instanceIds: string[]) => void;

const sanitizeId = (idWithRandom: string) => idWithRandom.replace(/-[^-]+$/, '');

/** ensures that only one interface ID implementation is present */
const interfaceUsedOnlyOnce: Validator = (cwds, instanceIds) => {
  const clashingWithExistingInstance = cwds.filter((cwd) => instanceIds.includes(cwd.id));
  if (clashingWithExistingInstance.length > 0) {
    throw new Error(
      `The following classes are clashing (have duplicate ID) with instances already present in the container: ${clashingWithExistingInstance.map((cwd) => cwd.cls.name)}`,
    );
  }

  const groupedById = groupBy(cwds, (cwd) => cwd.id);
  if (Object.values(groupedById).every((groupedCwds) => groupedCwds.length === 1)) {
    return;
  }

  const messages = Object.entries(groupedById).map(
    ([id, groupedCwds]) => `'${sanitizeId(id)}' (classes: ${groupedCwds.map((cwd) => cwd.cls.name).join(',')})`,
  );
  throw new Error(`The following interface IDs were used multiple times ${messages.join(',')}`);
};

/** throws an error if any class depends on an interface that is not available */
const dependenciesArePresent: Validator = (cwds, instanceIds) => {
  const allIds = new Set([...cwds.map((cwd) => cwd.id), ...instanceIds]);
  const cwsWithUnmetDeps = cwds.filter((cwd) => !cwd.dependencies.every((d) => allIds.has(d)));
  if (cwsWithUnmetDeps.length === 0) {
    return;
  }
  const messages = cwsWithUnmetDeps.map((cwd) => {
    const unmetDeps = cwd.dependencies.filter((d) => !allIds.has(d)).map(sanitizeId);
    return `Class ${cwd.cls.name} (interface ${sanitizeId(cwd.id)}) depends on interfaces [${unmetDeps}] that weren't added to the init method.`;
  });
  throw new Error(messages.join('\n'));
};

/** uses depth first search to find out if the classes have circular dependency */
const noCircularDependencies: Validator = (cwds, instanceIds) => {
  const inStack = new Set<string>();

  const hasCircularDependency = (id: string): boolean => {
    if (inStack.has(id)) {
      return true;
    }
    inStack.add(id);

    const cwd = cwds.find((c) => c.id === id);

    // we reference existing instance, there won't be circular dependency past this one because instances don't have any dependencies
    if (!cwd && instanceIds.includes(id)) {
      inStack.delete(id);
      return false;
    }

    if (!cwd) throw new Error(`assertion error: dependency ID missing ${sanitizeId(id)}`);

    for (const dependencyId of cwd.dependencies) {
      if (hasCircularDependency(dependencyId)) {
        return true;
      }
    }

    inStack.delete(id);
    return false;
  };

  for (const cwd of cwds) {
    if (hasCircularDependency(cwd.id)) {
      throw new Error(
        `Circular dependency detected between interfaces (${Array.from(inStack).map(sanitizeId)}), starting with '${sanitizeId(cwd.id)}' (class: ${cwd.cls.name}).`,
      );
    }
  }
};

/**
 * Topological sort of the dependencies - returns array of classes. The classes should be initialized in order given by the array.
 * https://en.wikipedia.org/wiki/Topological_sorting
 */
const sortDependencies = (classes: Map<string, ClassWithDependencies>): ClassWithDependencies[] => {
  const visited = new Set<string>();
  const sortedClasses: ClassWithDependencies[] = [];
  const topologicalSort = (interfaceId: string) => {
    if (visited.has(interfaceId)) {
      return;
    }
    visited.add(interfaceId);

    const cwd = classes.get(interfaceId);
    if (!cwd) {
      // the instance for this ID is already initiated
      return;
    }
    for (const d of cwd.dependencies || []) {
      topologicalSort(d);
    }
    sortedClasses.push(cwd);
  };

  for (const id of classes.keys()) {
    topologicalSort(id);
  }

  return sortedClasses;
};

/**
 * Helper type used by the brandInstance function to ensure that all container.addInstances parameters are branded
 */
export type BrandedInstance<T extends object> = T;

/**
 * use brandInstance to be able to pass this instance to the container.addInstances method
 */
export const brandInstance = <T extends object>(id: InterfaceId<T>, instance: T): BrandedInstance<T> => {
  injectableMetadata.set(instance, { id, dependencies: [] });
  return instance;
};

/**
 * Container is responsible for initializing a dependency tree.
 *
 * It receives a list of classes decorated with the `@Injectable` decorator
 * and it constructs instances of these classes in an order that ensures class' dependencies
 * are initialized before the class itself.
 */
export class Container {
  #instances = new Map<string, unknown>();

  /**
   * addInstances allows you to add pre-initialized objects to the container.
   * This is useful when you want to keep mandatory parameters in class' constructor (e.g. some runtime objects like server connection).
   * addInstances accepts a list of any objects that have been "branded" by the `brandInstance` method
   */
  addInstances(...instances: BrandedInstance<object>[]) {
    for (const instance of instances) {
      const metadata = injectableMetadata.get(instance);
      if (!metadata) {
        throw new Error(
          'assertion error: addInstance invoked without branded object, make sure all arguments are branded with `brandInstance`',
        );
      }
      if (this.#instances.has(metadata.id)) {
        throw new Error(
          `you are trying to add instance for interfaceId ${metadata.id}, but instance with this ID is already in the container.`,
        );
      }
      this.#instances.set(metadata.id, instance);
    }
  }

  /**
   * instantiate accepts list of classes, validates that they can be managed by the container
   * and then initialized them in such order that dependencies of a class are initialized before the class
   */
  instantiate(...classes: WithConstructor[]) {
    // ensure all classes have been decorated with @Injectable
    const undecorated = classes.filter((c) => !injectableMetadata.has(c)).map((c) => c.name);
    if (undecorated.length) {
      throw new Error(`Classes [${undecorated}] are not decorated with @Injectable.`);
    }

    const classesWithDeps: ClassWithDependencies[] = classes.map((cls) => {
      // we verified just above that all classes are present in the metadata map
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const { id, dependencies } = injectableMetadata.get(cls)!;
      return { cls, id, dependencies };
    });

    const validators: Validator[] = [interfaceUsedOnlyOnce, dependenciesArePresent, noCircularDependencies];
    validators.forEach((v) => v(classesWithDeps, Array.from(this.#instances.keys())));
    const classesById = new Map<string, ClassWithDependencies>();

    // Index classes by their interface id
    classesWithDeps.forEach((cwd) => {
      classesById.set(cwd.id, cwd);
    });

    // Create instances in topological order
    for (const cwd of sortDependencies(classesById)) {
      const args = cwd.dependencies.map((dependencyId) => this.#instances.get(dependencyId));
      // eslint-disable-next-line new-cap
      const instance = new cwd.cls(...args);
      this.#instances.set(cwd.id, instance);
    }
  }

  get<T>(id: InterfaceId<T>): T {
    const instance = this.#instances.get(id);
    if (!instance) {
      throw new Error(`Instance for interface '${sanitizeId(id)}' is not in the container.`);
    }
    return instance as T;
  }
}
