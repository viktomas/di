export const groupBy = <T, K extends keyof any>(arr: T[], keyFn: (item: T) => K): Record<K, T[]> => {
  return arr.reduce(
    (acc, item) => {
      const key = keyFn(item);
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(item);
      return acc;
    },
    {} as Record<K, T[]>,
  );
};
