import {
  BrandedInstance,
  Container,
  Injectable,
  brandInstance,
  createInterfaceId,
} from './dependency';

describe('Container', () => {
  interface A {
    a(): string;
  }

  interface B {
    b(): string;
  }

  interface C {
    c(): string;
  }

  const A = createInterfaceId<A>('A');
  const B = createInterfaceId<B>('B');
  const C = createInterfaceId<C>('C');

  @Injectable(A, [])
  class AImpl implements A {
    a = () => 'a';
  }

  @Injectable(B, [A])
  class BImpl implements B {
    #a: A;

    constructor(a: A) {
      this.#a = a;
    }

    b = () => `B(${this.#a.a()})`;
  }

  @Injectable(C, [A, B])
  class CImpl implements C {
    #a: A;

    #b: B;

    constructor(a: A, b: B) {
      this.#a = a;
      this.#b = b;
    }

    c = () => `C(${this.#b.b()}, ${this.#a.a()})`;
  }

  let container: Container;

  beforeEach(() => {
    container = new Container();
  });

  describe('addInstances', () => {
    const O = createInterfaceId<object>('object');

    it('fails if the instance is not branded', () => {
      expect(() => container.addInstances({ say: 'hello' } as BrandedInstance<object>)).toThrow(
        /invoked without branded object/,
      );
    });

    it('fails if the instance is already present', () => {
      const a = brandInstance(O, { a: 'a' });
      const b = brandInstance(O, { b: 'b' });
      expect(() => container.addInstances(a, b)).toThrow(/this ID is already in the container/);
    });

    it('adds instance', () => {
      const instance = { a: 'a' };
      const a = brandInstance(O, instance);
      container.addInstances(a);

      expect(container.get(O)).toBe(instance);
    });
  });

  describe('instantiate', () => {
    it('can instantiate three classes A,B,C', () => {
      container.instantiate(AImpl, BImpl, CImpl);

      const cInstance = container.get(C);
      expect(cInstance.c()).toBe('C(B(a), a)');
    });

    it('instantiates dependencies in multiple instantiate calls', () => {
      container.instantiate(AImpl);
      // the order is important for this test
      // we want to make sure that the stack in circular dependency discovery is being cleared
      // to try why this order is necessary, remove the `inStack.delete()` from
      // the `if (!cwd && instanceIds.includes(id))` condition in prod code
      expect(() => container.instantiate(CImpl, BImpl)).not.toThrow();
    });

    it('detects duplicate ids', () => {
      @Injectable(A, [])
      class AImpl2 implements A {
        a = () => 'hello';
      }

      expect(() => container.instantiate(AImpl, AImpl2)).toThrow(
        /The following interface IDs were used multiple times 'A' \(classes: AImpl,AImpl2\)/,
      );
    });

    it('detects duplicate id with pre-existing instance', () => {
      const aInstance = new AImpl();
      const a = brandInstance(A, aInstance);
      container.addInstances(a);
      expect(() => container.instantiate(AImpl)).toThrow(/classes are clashing/);
    });

    it('detects missing dependencies', () => {
      expect(() => container.instantiate(BImpl)).toThrow(
        /Class BImpl \(interface B\) depends on interfaces \[A]/,
      );
    });

    it('it uses existing instances as dependencies', () => {
      const aInstance = new AImpl();
      const a = brandInstance(A, aInstance);
      container.addInstances(a);

      container.instantiate(BImpl);

      expect(container.get(B).b()).toBe('B(a)');
    });

    it("detects classes what aren't decorated with @Injectable", () => {
      class AImpl2 implements A {
        a = () => 'hello';
      }

      expect(() => container.instantiate(AImpl2)).toThrow(
        /Classes \[AImpl2] are not decorated with @Injectable/,
      );
    });

    it('detects circular dependencies', () => {
      @Injectable(A, [C])
      class ACircular implements A {
        a = () => 'hello';

        constructor(c: C) {
          // eslint-disable-next-line no-unused-expressions
          c;
        }
      }

      expect(() => container.instantiate(ACircular, BImpl, CImpl)).toThrow(
        /Circular dependency detected between interfaces \(A,C\), starting with 'A' \(class: ACircular\)./,
      );
    });

    // this test ensures that we don't store any references to the classes and we instantiate them only once
    it('does not instantiate classes from previous instantiate call', () => {
      let globCount = 0;

      @Injectable(A, [])
      class Counter implements A {
        counter = globCount;

        constructor() {
          globCount++;
        }

        a = () => this.counter.toString();
      }

      container.instantiate(Counter);
      container.instantiate(BImpl);

      expect(container.get(A).a()).toBe('0');
    });
  });

  describe('get', () => {
    it('returns an instance of the interfaceId', () => {
      container.instantiate(AImpl);

      expect(container.get(A)).toBeInstanceOf(AImpl);
    });
    it('throws an error for missing dependency', () => {
      container.instantiate();

      expect(() => container.get(A)).toThrow(/Instance for interface 'A' is not in the container/);
    });
  });
});
