# needle

Mini dependency injection framework using the ES stage 3 decorators.

See the [src/index.ts](src/index.ts) for example usage and [src/dependency.ts](src/dependency.ts) for implementation.

## Key features

- Uses the new decorators standard (the TS experimental decorators are now [marked as legacy](https://devblogs.microsoft.com/typescript/announcing-typescript-5-0/#differences-with-experimental-legacy-decorators) and even though still supported, it's recommended to use the standard going forward)
- Works out-of-the-box with latest TypeScript and `esbuild`. No special build flags are necessary
- Fully type safe - ensures that the decorated `Injectable` dependencies have correct type and constructor arguments match the dependences.

## Try it out

1. Check out this repository.
1. Make sure you use `asdf` to install the local `nodejs` version.
1. Install dependencies (`npm i`)
1. Run the example from `index.ts` with `npm run start`

## Basic usage

You define your interfaces

```ts
interface A {
  field: string;
}

interface B {
  hello(): string;
}
```

You define interface IDs

```ts
const A = createInterfaceId<A>('A');
const B = createInterfaceId<B>('B');
```

You create implementations and decorate them

```ts
@Injectable(A, [])
class AImpl implements A {
  field = 'value';
}

@Injectable(B, [A])
class BImpl implements B {
  #a: A;

  constructor(a: A) {
    this.#a = a;
  }

  hello() {
    return `B.hello says A.field = ${this.#a.field}`;
  }
}
```

Initialize container

```ts
const container = new Container();
container.instantiate(AImpl, BImpl);
```

Retrieve dependency and use it

```ts
const b = container.get(B);
console.log(b.hello());
```

## Initializing dependencies outside of the container

Some classes might require objects that the container can't provide (sockets, io, legacy classes). For these classes, you can can create them outside of the container and add them using the `addInstances` method.

```ts
interface Logger {
  log(...args: unknown[]): void;
}

const Logger = createInterfaceId<Logger>('Logger');

const customLogger: Logger = {
  log: (...args) => console.log('custom logger', ...args),
};

container.addInstances(brandInstance(Logger, customLogger));
```

After adding `Logger`, you can call `container.instantiate()` with any `Injectable` classes that need `Logger` in their constructor.

## Why didn't you decorate the constructor parameters

Because you can't, the ES Stage 3 decorators don't support decorating method/function parameters.

- [See stage 1 proposal for parameter decorators](https://docs.google.com/document/d/1Qpkqf_8NzAwfD8LdnqPjXAQ2wwh8BBUGynhn-ZlCWT0/edit#heading=h.t9k9f05noi8w)

## How does it work under the hood

- The most important implementation is the type juggling on the `Injectable` decorator. That is the part that makes this mini framework type safe.
- The `Injectable` decorator adds `__interfaceId` and `__dependencies` attributes on the decorated class prototype.
- The container initialization then works with strings and classes, not trying to be too typed. It assumes that if the `__interfaceId` attribute has been set on the prototype, the class was type safely decorated with `Injectable`.

## More resources

- [Decorator types for TypeScript](https://github.com/microsoft/TypeScript/pull/50820)
- [Reference documentation](https://2ality.com/2022/10/javascript-decorators.html#example-friend-visibility)
